package com.sapient.restaurant.client;

import java.net.URI;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sapient.restaurant.bean.RestaurantDetails;

import org.glassfish.jersey.client.ClientConfig;

public class RestaurantTracker {

	private static URI getLocation(String city)
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/locations?query="+city).build();
	}
	private static URI getLocationDetails(String entity_type,int entity_id)
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/location_details?entity_id="+entity_id+"&entity_type="+entity_type).build();
	}
	private static URI getRestaurant(int res_id)
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/restaurant?res_id="+res_id).build();
	}
	public static Map<String,RestaurantDetails> findRestaurants(String city) throws JSONException 
	{
		Map<String,RestaurantDetails> map = new HashMap<String,RestaurantDetails>();
		ClientConfig config = new ClientConfig();
		Client client= ClientBuilder.newClient(config);
		WebTarget target= client.target(getLocation(city));
		String jsonString=target.request().header("user-key","b22c755aa188f4eeff7906f4e567a460").accept(MediaType.APPLICATION_JSON).get(String.class);
		JSONObject obj = new JSONObject(jsonString);
		JSONArray array=(JSONArray)obj.get("location_suggestions");
		JSONObject objn=(JSONObject)array.get(0);
		String entity_type=(String)objn.get("entity_type");
		int entity_id=(int)objn.get("entity_id");
		WebTarget target2= client.target(getLocationDetails(entity_type,entity_id));
		String jsonString2=target2.request().header("user-key","b22c755aa188f4eeff7906f4e567a460").accept(MediaType.APPLICATION_JSON).get(String.class);
		JSONObject obj2 = new JSONObject(jsonString2);
		JSONArray array2=(JSONArray)obj2.get("nearby_res");
		WebTarget target3;
		for(int i=0;i<array2.length();i++)
		{
			String resid=array2.getString(i);
			target3=client.target(getRestaurant((int)array2.getInt(i)));
			String jsonString3=target3.request().header("user-key","b22c755aa188f4eeff7906f4e567a460").accept(MediaType.APPLICATION_JSON).get(String.class);
			JSONObject obj3 = new JSONObject(jsonString3);
			String name=obj3.getString("name");
			String cuisines=obj3.getString("cuisines");
			String timings=obj3.getString("timings");
			JSONObject obj4=obj3.getJSONObject("location");
			String address=obj4.getString("address");
			String locality=obj4.getString("locality");
			RestaurantDetails resd= new RestaurantDetails(resid, name, address, locality, cuisines, timings);
			map.put(resid,resd);
		}
		return map;
	}
}
