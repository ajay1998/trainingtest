import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
function Square(props) {
  return (
    <React.Fragment>
      <button
        className="btn btn-outline-success"
        style={{ height: 100, width: 100 }}
        onClick={props.onClick}
      >
        {props.value}
      </button>
    </React.Fragment>
  );
}

export default Square;
