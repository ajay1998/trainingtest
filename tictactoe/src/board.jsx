import React, { Component } from "react";
import Square from "./square";

class Board extends Component {
  state = {
    svalue: true,
    sqstate: [null, null, null, null, null, null, null, null, null]
  };
  checkCondition() {
    let arr = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

    for (let i = 0; i < arr.length; i++) {
      if (
        this.state.sqstate[arr[i][0]] === this.state.sqstate[arr[i][1]] &&
        this.state.sqstate[arr[i][0]] === this.state.sqstate[arr[i][2]] &&
        this.state.sqstate[arr[i][2]] === this.state.sqstate[arr[i][1]] &&
        this.state.sqstate[arr[i][0]] !== null
      ) {
        return alert("Winner is Palyer " + this.state.sqstate[arr[i][0]]);
      }
    }
  }
  handleMark(i) {
    if (this.state.svalue === true) {
      let { sqstate } = this.state;
      let num = parseInt(i);
      sqstate[num] = "X";
      this.setState({ sqstate: sqstate });
      this.checkCondition();
      this.setState({ svalue: false });
    } else {
      let { sqstate } = this.state;
      let num = parseInt(i);
      sqstate[num] = "O";
      this.setState({ sqstate: sqstate });
      this.checkCondition();
      this.setState({ svalue: true });
    }
  }
  render() {
    return (
      <React.Fragment>
        <Square
          value={this.state.sqstate[0]}
          onClick={() => this.handleMark(0)}
        />
        <Square
          value={this.state.sqstate[1]}
          onClick={() => this.handleMark(1)}
        />
        <Square
          value={this.state.sqstate[2]}
          onClick={() => this.handleMark(2)}
        />
        <br />
        <Square
          value={this.state.sqstate[3]}
          onClick={() => this.handleMark(3)}
        />
        <Square
          value={this.state.sqstate[4]}
          onClick={() => this.handleMark(4)}
        />
        <Square
          value={this.state.sqstate[5]}
          onClick={() => this.handleMark(5)}
        />
        <br />
        <Square
          value={this.state.sqstate[6]}
          onClick={() => this.handleMark(6)}
        />
        <Square
          value={this.state.sqstate[7]}
          onClick={() => this.handleMark(7)}
        />
        <Square
          value={this.state.sqstate[8]}
          onClick={() => this.handleMark(8)}
        />
      </React.Fragment>
    );
  }
}

export default Board;
