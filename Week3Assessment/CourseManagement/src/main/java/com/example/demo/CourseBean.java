package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CourseBean {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int course_id;
	@Column
	private String course_title;
	@Column
	private String start_date;
	@Column
	private String end_date;
	@Column
	private int fees;
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public String getCourse_title() {
		return course_title;
	}
	public void setCourse_title(String course_title) {
		this.course_title = course_title;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public int getFees() {
		return fees;
	}
	public void setFees(int fees) {
		this.fees = fees;
	}
	public CourseBean() {
		super();
	}
	public CourseBean(String course_title, String start_date, String end_date, int fees) {
		super();
		this.course_title = course_title;
		this.start_date = start_date;
		this.end_date = end_date;
		this.fees = fees;
	}
}
