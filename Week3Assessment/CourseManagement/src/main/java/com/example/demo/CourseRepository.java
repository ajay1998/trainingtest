package com.example.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface CourseRepository extends CrudRepository<CourseBean, Integer> {

}
