package com.example.demo;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

class Read
{
public static Scanner sc=new Scanner(System.in);	
}
@RestController
public class Controller {

	@Autowired
	CourseRepository crep;
	
	@PostMapping("insert")
	public void insertCourses()
	{
		CourseBean cbean= new CourseBean("DBMS","15/06/2016","08/12/2016",2000);
		crep.save(cbean);
	}
	
	@GetMapping("get")
	public List<CourseBean> getCourses()
	{
		return (List<CourseBean>)crep.findAll();
	}
	
	@GetMapping("getcourse/{id}")
	public Optional<CourseBean> getCourses(@PathVariable("id") int course_id)
	{
		return crep.findById(course_id);
	}
	@RequestMapping("delete/{id}")
	public void deleteCourses(@PathVariable("id") int course_id)
	{
		crep.deleteById(course_id);
	}
}
