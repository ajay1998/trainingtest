package com.sapient.restaurant.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BookPage
 */
public class BookPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String resName=request.getParameter("resName");
			RequestDispatcher rd = request.getRequestDispatcher("booking.jsp");
			request.setAttribute("resName", resName);
			rd.forward(request, response);
		}catch(Exception e) {}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String name=request.getParameter("name");
			String resName=request.getParameter("resName");
			String date=request.getParameter("date");
			String time=request.getParameter("time");
			String seats=request.getParameter("seats");
			RequestDispatcher rd = request.getRequestDispatcher("booking.jsp");
			
			rd.forward(request, response);
		}catch(Exception e) {}
	}

}
