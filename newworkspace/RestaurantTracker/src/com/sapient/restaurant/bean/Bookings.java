package com.sapient.restaurant.bean;

public class Bookings {
	private String bookid;
	private String name;
	private String date;
	private String resName;
	private String seats;
	private String time;
	public String getName() {
		return name;
	}
	public Bookings() {
		super();
	}

	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public Bookings(String bookid, String name, String date, String resName, String seats, String time) {
		super();
		this.bookid = bookid;
		this.name = name;
		this.date = date;
		this.resName = resName;
		this.seats = seats;
		this.time = time;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getResName() {
		return resName;
	}
	public void setResName(String resName) {
		this.resName = resName;
	}
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}
