package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			out.print("<html><center><body><h1>Calculator</h1>");
			out.print("<form action='TestServlet' method='post'>");
			out.print("<br>Enter 1st number <input type='text' name='a1' value='0'>");
			out.print("<br>Enter 2nd number <input type='text' name='a2' value='0'>");
			out.print("<br>Result <input type='text' name='a3' value='0'>");
			out.print("<br><input type='radio' name='op' value='0'>ADD");
			out.print("<br><input type='radio' name='op' value='1'>SUB");
			out.print("<br><input type='radio' name='op' value='2'>MUL");
			out.print("<br><input type='radio' name='op' value='3'>DIV");
			out.print("<br><input type='submit'  value='Calculate'>");
			out.print("</form></body></center></html>");
		}
		catch(Exception e)
		{
			
		}
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			int n1=Integer.parseInt(request.getParameter("a1"));
			int n2=Integer.parseInt(request.getParameter("a2"));
			int n3=0;
			int op=Integer.parseInt(request.getParameter("op"));
			if(op==0)
				n3=n1+n2;
			else if(op==1)
				n3=n1-n2;
			else if(op==2)
				n3=n1*n2;
			else if(op==3)
				n3=n1/n2;
			out.print("<html><center><body><h1>Calculator</h1>");
			out.print("<form action='TestServlet' method='post'>");
			out.print("<br>Enter 1st number <input type='text' name='a1' value='"+n1+"'>");
			out.print("<br>Enter 2nd number <input type='text' name='a2' value='"+n2+"'>");
			out.print("<br>Result <input type='text' name='a3' value='"+n3+"'>");
			out.print("<br><input type='radio' name='op' value='0'>ADD");
			out.print("<br><input type='radio' name='op' value='1'>SUB");
			out.print("<br><input type='radio' name='op' value='2'>MUL");
			out.print("<br><input type='radio' name='op' value='3'>DIV");
			out.print("<br><input type='submit'  value='Calculate'>");
			out.print("</form></body></center></html>");
		}
		catch(Exception e)
		{
			
		}
	}

}
