package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EditPage
 */
public class EditPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			int logid=Integer.parseInt(request.getParameter("logid"));
			int lvl=Integer.parseInt(request.getParameter("lvl"));
			int logid1=Integer.parseInt(request.getParameter("slogid"));
			StudentRecord stud=DAOClass.getStudent(logid1);
			int roll=stud.getRoll();
			String fname=stud.getFname();
			String lname=stud.getLname();
			float marks=stud.getMarks();
			String year=stud.getYear();
			String i="<html><body><a href='HomePage?logid="+logid+"&lvl="+lvl+"'>Home</a><br>"
					+ "<a href='LoginPage'>Logout</a><br><center><h1>EditPageForm</h1><br><form action='ListPage?logid="+logid+"&lvl="+lvl+"&slogid="+logid1+"' method='post'>"
					+ "<br><input type='text' name='roll' value='"+roll+"'>"
					+ "<br><input type='text' name='fname' value='"+fname+"'>"
					+ "<br><input type='text' name='lname' value='"+lname+"'>"
					+ "<br><input type='text' name='marks' value='"+marks+"'>"
					+ "<br><input type='text' name='year' value='"+year+"'>"
					+ "<br><br><input type='submit' value='Submit'></form></center></body></html>";
			out.print(i);
		}
		catch(Exception e)
		{}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
