package com.example.demo;

import java.util.Arrays;

import javax.ws.rs.core.MediaType;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public class AddService {
	static final String URL="http://localhost:8099/post"; 
	public static void main(String[] args) {
		HttpHeaders headers = new HttpHeaders();
		 headers.add("Accept", MediaType.APPLICATION_JSON);
         headers.set("ContentType",MediaType.APPLICATION_JSON);	}
	 	RestTemplate restTemplate = new RestTemplate();
	 	ProfileBean pbean = new ProfileBean(1,"ajay","sabale","23/04/98","Software Eng.");
     
     HttpEntity<ProfileBean> requestBody = new HttpEntity<ProfileBean>(pbean,headers);

   
    ProfileBean e = restTemplate.postForObject(URL, requestBody,ProfileBean.class);

}
