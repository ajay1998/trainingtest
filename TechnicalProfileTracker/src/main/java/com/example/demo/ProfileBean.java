package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class ProfileBean {
	@Id
	private int Id;
	private String fname;
	private String lname;
	private String dob;
	private String profile;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public ProfileBean(int id, String fname, String lname, String dob, String profile) {
		super();
		Id = id;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.profile = profile;
	}
	public ProfileBean() {
		super();
	}
	
}
