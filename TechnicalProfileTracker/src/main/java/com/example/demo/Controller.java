package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
 
	@Autowired
	ProfileClass pfc;
	@PostMapping("post")
	public void addProfiles()
	{
		ProfileBean pbean = new ProfileBean(1,"ajay","sabale","23/04/98","Software Eng.");
		pfc.save(pbean);
	}
	@GetMapping("get")
	public List<ProfileBean> getProfiles()
	{
		return (List<ProfileBean>) pfc.findAll();
	}
}
