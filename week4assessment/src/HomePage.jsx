import React from "react";
import ReactDOM from "react-dom";
import { MDBRow, MDBCol, MDBBtn } from "mdbreact";
import "bootstrap/dist/css/bootstrap.css";
import NextPage from "./NextPage";
class FormsPage extends React.Component {
  state = {
    name: "",
    dob: "",
    email: "",
    city: "",
    valok: true
  };

  submitHandler = event => {
    event.preventDefault();
    if (event.target.name.value === null || event.target.name.value === "") {
      this.setState({ valok: false });
      ReactDOM.render(
        <h5>Empty Field</h5>,
        document.getElementById("namefield")
      );
    }

    if (this.state.valok === true) {
      console.log("ok");
      return (
        <NextPage
          name={this.state.name}
          email={this.state.email}
          dob={this.state.dob}
          city={this.state.city}
        />
      );
    }
  };

  changeHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.submitHandler} noValidate>
          <MDBRow>
            <MDBCol md="4" className="mb-3">
              <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
                Name
              </label>
              <input
                value={this.state.name}
                name="name"
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterNameEx"
                className="form-control"
                placeholder="name"
              />
              <div id="namefield" />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="4" className="mb-3">
              <label htmlFor="date" className="grey-text">
                DOB
              </label>
              <input
                value={this.state.dob}
                name="dob"
                onChange={this.changeHandler}
                type="date"
                id="defaultFormRegisterEmailEx2"
                className="form-control"
                placeholder="DOB"
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterConfirmEx3"
                className="grey-text"
              >
                Email
              </label>
              <input
                value={this.state.email}
                onChange={this.changeHandler}
                type="email"
                id="defaultFormRegisterConfirmEx3"
                className="form-control"
                name="email"
                placeholder="Your Email address"
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterPasswordEx4"
                className="grey-text"
              >
                City
              </label>
              <select
                value={this.state.city}
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterPasswordEx4"
                className="form-control"
                name="city"
              >
                <option value="Pune">Pune</option>
                <option value="Gurgaon">Gurgaon</option>
                <option value="Mumbai">Mumbai</option>
                <option value="New Delhi">New Delhi</option>
              </select>

              <div className="invalid-feedback">
                Please provide a valid city.
              </div>
              <div className="valid-feedback">Looks good!</div>
            </MDBCol>
          </MDBRow>

          <MDBBtn color="primary" type="submit">
            Submit Form
          </MDBBtn>
        </form>
      </div>
    );
  }
}

export default FormsPage;
