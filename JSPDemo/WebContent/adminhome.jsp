<%@page import="com.sapient.student.bean.StudentRecord"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<a href='LogoutPage'>Logout</a>
<%
String type="";
try{
type=(String)request.getAttribute("type");
}
catch(Exception ex)
{type="";}
%>
<center>
<br><a href='InsertPage'>Insert</a>
<br><a href='ListPage'>List</a>
<br>
<br>
<%if(type.equals("1")){ %>
<form action='InsertPage' method='post'>
<br><input type='text' name='logid' placeholder='LoginID'>
<br><input type='text' name='roll' placeholder='Roll No'>
<br><input type='text' name='fname' placeholder='FirstName'>
<br><input type='text' name='lname' placeholder='LastName'>
<br><input type='text' name='marks' placeholder='Marks'>
<br><input type='text' name='year' placeholder='Year'>
<br><br><input type='submit' value='Insert'></form>
<%}%>
<%if(type.equals("2")){ 
List<StudentRecord> list=(List<StudentRecord>)request.getAttribute("list");%>
<table border="1"><tr><th>LoginID</th>
<th>Roll No</th>
<th>FirstName</th>
<th>LastName</th>
<th>Marks</th>
<th>Class</th>
<th>Edit</th>
<th>Delete</th></tr>
<% for(StudentRecord s: list){%>
<tr><td><%=s.getLogid()%></td><td><%=s.getRoll()%></td><td><%=s.getFname()%></td><td><%=s.getLname()%></td><td><%=s.getMarks()%></td><td><%=s.getYear()%></td><td><a href='EditPage?logid=<%=s.getLogid()%>'>Edit</a></td><td><a href='ListPage?logid=<%=s.getLogid()%>'>Delete</a></td></tr>
<% }%>
</table>
<%}%>

</center>
</body>
</html>