package com.sapient.student.bundles;

import java.util.ResourceBundle;

public class RBundle {
public static String getValue(String key)
{
	ResourceBundle rbundle=ResourceBundle.getBundle("student");
	return rbundle.getString(key);
}
}