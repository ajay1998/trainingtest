package com.sapient.student.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.dao.DAOClass;

/**
 * Servlet implementation class InsertPage
 */
public class InsertPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
		RequestDispatcher rd=request.getRequestDispatcher("adminhome.jsp");
		request.setAttribute("type","1");
		rd.forward(request, response);
		}
		catch(Exception e)
		{}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			int logid=Integer.parseInt(request.getParameter("logid"));
			int roll =Integer.parseInt(request.getParameter("roll"));
			String fname=request.getParameter("fname");
			String lname=request.getParameter("lname");
			float marks=Float.parseFloat(request.getParameter("marks"));
			String year=request.getParameter("year");
			DAOClass.insert(logid, roll, fname, lname, marks, year);
			RequestDispatcher rd=request.getRequestDispatcher("adminhome.jsp");
			request.setAttribute("type","1");
			rd.forward(request, response);
		}
		catch(Exception e)
		{}		
	}

}
