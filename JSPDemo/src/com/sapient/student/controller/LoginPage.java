package com.sapient.student.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.LoginDetails;
import com.sapient.student.bundles.RBundle;
import com.sapient.student.dao.DAOClass;
import com.sapient.student.exception.AccountInvalidException;
import com.sapient.student.exception.GeneralException;

/**
 * Servlet implementation class LoginPage
 */
public class LoginPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginPage() {
        super();
        // TODO Auto-generated constructor stub
    }
    ServletContext cn;
    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	
    	try {
			cn=config.getServletContext();
			cn.setAttribute("studentlist", DAOClass.display());
			super.init(config);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			String logid=request.getParameter("logid");
			if(logid.equals(""))
			{
				throw new GeneralException(RBundle.getValue("Error1"));
			}
			String password=request.getParameter("password");
			if(password.equals(""))
			{
				throw new GeneralException(RBundle.getValue("Error1"));
			}
			LoginDetails lod=DAOClass.getCredentials(Integer.parseInt(logid));
			if(lod==null||(!(lod.getLogid().equals(logid)&&lod.getPassword().equals(password))))
			{
				throw new AccountInvalidException(RBundle.getValue("Error2"));
			}
			else
			{
				if(Integer.parseInt(lod.getLvl())==1)
				{	
					RequestDispatcher rd=request.getRequestDispatcher("adminhome.jsp");
					request.setAttribute("type","0");
					rd.forward(request, response);
				}
				else
				{
					RequestDispatcher rd=request.getRequestDispatcher("studenthome.jsp");
					//request.setAttribute("lvl",lod.getLvl());
					rd.forward(request, response);
				}
			}
		}
		catch(GeneralException ex)
		{
			RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
			request.setAttribute("err1",ex.getMessage() );
			rd.forward(request, response);
		}
		catch(AccountInvalidException ex)
		{
			RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
			request.setAttribute("err2",ex.getMessage() );
			rd.forward(request, response);
		}
	}

}