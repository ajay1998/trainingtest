package com.sapient.student.controller;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.management.relation.RoleInfoNotFoundException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.LoginDetails;
import com.sapient.student.bean.StudentRecord;
import com.sapient.student.bundles.RBundle;
import com.sapient.student.dao.DAOClass;
import com.sapient.student.exception.AccountInvalidException;
import com.sapient.student.exception.GeneralException;
import com.sapient.student.exception.RollNoInvalidException;

/**
 * Servlet implementation class MarksPage
 */
public class MarksPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MarksPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		try {
			String roll=request.getParameter("roll");
			System.out.println(roll);
			if(roll.equals("null")||roll.equals(""))
			{
				throw new GeneralException(RBundle.getValue("Error1"));
			}
			StudentRecord stud=DAOClass.getStudent(Integer.parseInt(roll));
			if(stud==null)
			{
				throw new RollNoInvalidException(RBundle.getValue("Error3"));
			}
			else
			{
				    
					RequestDispatcher rd=request.getRequestDispatcher("studenthome.jsp");
					request.setAttribute("marks",DAOClass.getmarks(Integer.parseInt(roll)));
					request.setAttribute("roll",roll);
					rd.forward(request, response);
				
			}
		}
		catch(GeneralException ex)
		{
			RequestDispatcher rd=request.getRequestDispatcher("studenthome.jsp");
			request.setAttribute("err1",ex.getMessage() );
			rd.forward(request, response);
		}
		catch(RollNoInvalidException ex)
		{
			RequestDispatcher rd=request.getRequestDispatcher("studenthome.jsp");
			request.setAttribute("err2",ex.getMessage() );
			rd.forward(request, response);
		}
	
	}

}
