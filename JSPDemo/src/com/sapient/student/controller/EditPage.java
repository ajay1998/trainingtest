package com.sapient.student.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.StudentRecord;
import com.sapient.student.dao.DAOClass;

/**
 * Servlet implementation class EditPage
 */
public class EditPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			String logid=request.getParameter("logid");
			StudentRecord stud=DAOClass.getDataForEdit(Integer.parseInt(logid));
			String roll=stud.getRoll();
			String fname=stud.getFname();
			String lname=stud.getLname();
			String marks=stud.getMarks();
			String year=stud.getYear();
			RequestDispatcher rd=request.getRequestDispatcher("edit.jsp");
			request.setAttribute("logid",logid);
			request.setAttribute("roll",roll);
			request.setAttribute("fname",fname);
			request.setAttribute("lname",lname);
			request.setAttribute("marks",marks);
			request.setAttribute("year",year);
			rd.forward(request, response);
			}
			catch(Exception e)
			{}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
