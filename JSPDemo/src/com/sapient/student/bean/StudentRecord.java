package com.sapient.student.bean;

public class StudentRecord {
	private String logid;
	private String roll;
	private String fname;
	private String lname;
	private String marks;
	private String year;
	public StudentRecord() {
		super();
	}
	public StudentRecord(String logid, String roll, String fname, String lname, String marks, String year) {
		super();
		this.logid = logid;
		this.roll = roll;
		this.fname = fname;
		this.lname = lname;
		this.marks = marks;
		this.year = year;
	}
	
	public String getLogid() {
		return logid;
	}
	public void setLogid(String logid) {
		this.logid = logid;
	}
	public String getRoll() {
		return roll;
	}
	
	public void setRoll(String roll) {
		this.roll = roll;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getMarks() {
		return marks;
	}
	public void setMarks(String marks) {
		this.marks = marks;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
}