package com.sapient.student.bean;


public class LoginDetails {
	private String logid;
	private String password;
	private String lvl;
	public LoginDetails() {
		super();
	}
	public LoginDetails(String logid, String password, String lvl) {
		super();
		this.logid = logid;
		this.password = password;
		this.lvl = lvl;
	}
	public String getLogid() {
		return logid;
	}
	public void setLogid(String logid) {
		this.logid = logid;
	}
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLvl() {
		return lvl;
	}
	public void setLvl(String lvl) {
		this.lvl = lvl;
	}
	@Override
	public String toString() {
		return "LoginDetails [logid=" + logid + ", password=" + password + ", lvl=" + lvl + "]";
	}
}