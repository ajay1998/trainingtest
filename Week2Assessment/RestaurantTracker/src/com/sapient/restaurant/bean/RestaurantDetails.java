package com.sapient.restaurant.bean;

public class RestaurantDetails {
	private String resid;
	private String name;
	private String address;
	private String locality;
	private String cuisines;
	private String timings;
	
	public RestaurantDetails() {
		super();
	}
	public RestaurantDetails(String resid, String name, String address, String locality, String cuisines,
			String timings) {
		super();
		this.resid = resid;
		this.name = name;
		this.address = address;
		this.locality = locality;
		this.cuisines = cuisines;
		this.timings = timings;
	}
	public String getResid() {
		return resid;
	}
	public void setResid(String resid) {
		this.resid = resid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getCuisines() {
		return cuisines;
	}
	public void setCuisines(String cuisines) {
		this.cuisines = cuisines;
	}
	public String getTimings() {
		return timings;
	}
	public void setTimings(String timings) {
		this.timings = timings;
	}
}
