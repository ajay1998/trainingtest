import React, { Component } from "react";

class Counter extends Component {
  state = {
    age: 0,
    name: "",
    city: "",
    emp: [
      {
        name: "ajay",
        city: "satara"
      }
    ]
  };
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({
      emp: this.state.emp.push({ name: this.state.name, city: this.state.city })
    });
    console.log(this.state.emp);
  }

  render() {
    return (
      <React.Fragment>
        <ul>
          {this.state.emp.map(tag => (
            <li key={tag.name}>
              {tag.name} {tag.city}
            </li>
          ))}
        </ul>
        {/* <table>
          <tr>
            <th>Name</th>
            <th>City</th>
          </tr>
          <tr>
            <td>{this.state.emp.name}</td>
            <td>{this.state.emp.city}</td>
          </tr>
        </table> */}
        {this.state.emp}
        Age:{this.state.age}
        <form onSubmit={this.handleSubmit.bind(this)}>
          <input
            type="text"
            name="name"
            onChange={this.handleChange.bind(this)}
          />
          <input
            type="text"
            name="city"
            onChange={this.handleChange.bind(this)}
          />
          <input type="submit" value="Click" />
        </form>
        <br />
      </React.Fragment>
    );
  }
}

export default Counter;
