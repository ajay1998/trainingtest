import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Counter from "./components/counter";

class Navbar extends Component {
  state = {};

  render() {
    return (
      <React.Fragment>
        <Router>
          <nav className="navbar navbar-expand-lg  bg-dark">
            <Link className="ml-2" to="/demo1">
              Demo1
            </Link>
            <Link className="ml-2" to="/demo2">
              Demo2
            </Link>
          </nav>
          <Route exact path="/demo1" component={this.f1} />
          <Route exact path="/demo2" component={this.f2} />
        </Router>
      </React.Fragment>
    );
  }
  f1() {
    return <Counter />;
  }
  f2() {
    return <Counter />;
  }
}

export default Navbar;
