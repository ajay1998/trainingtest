import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import Navbar from "./Render";

ReactDOM.render(<Navbar />, document.getElementById("root"));
