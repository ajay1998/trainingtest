package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo2 {

	public static void main(String[] args) {
		ApplicationContext context= new AnnotationConfigApplicationContext(JavaContainer.class);
		Holiday ob;
		ob=(Holiday)context.getBean(Holiday.class);
		System.out.println(ob);
		ListHolidays obj;
		obj=(ListHolidays)context.getBean(ListHolidays.class);
		System.out.println(obj);
}

}
