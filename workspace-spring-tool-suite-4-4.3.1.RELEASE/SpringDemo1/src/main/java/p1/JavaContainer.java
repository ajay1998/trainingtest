package p1;
	
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {

	@Bean
	@Scope("singleton")
	public Hello get1() {
		return  new Hello();
	}
	
	@Bean
	@Scope("prototype")
	public Holiday get()
	{
		return new Holiday("21/19","Maldives");
	}
	@Bean
	@Scope("prototype")
	public ListHolidays get2()
	{
		ListHolidays list =new ListHolidays();
		list.getList().add(new Holiday("21/19","Goa"));
		list.getList().add(new Holiday("22/20","Andaman"));
		return list;
	}
}
