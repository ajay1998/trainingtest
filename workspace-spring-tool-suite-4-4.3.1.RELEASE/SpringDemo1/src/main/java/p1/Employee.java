package p1;

public class Employee {
private String name;
private int age;
@Override
public String toString() {
	return "Employee [name=" + name + ", age=" + age + "]";
}
public Employee() {
	super();
}
public Employee(String name, int age) {
	super();
	this.name = name;
	this.age = age;
}
}
