package p1;

import java.util.ArrayList;
import java.util.List;

public class ListHolidays {
private List<Holiday> list = new ArrayList<Holiday>();

public List<Holiday> getList() {
	return list;
}

public void setList(List<Holiday> list) {
	this.list = list;
}

@Override
public String toString() {
	return "ListHolidays [list=" + list + "]";
}

public ListHolidays(List<Holiday> list) {
	super();
	this.list = list;
}

public ListHolidays() {
	super();
}

}


