package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(2)
public class ArithmeticAspect {
	
	
	@Before("execution( * *.*(double,double))")
	public void check1(JoinPoint jpoint)
	{
		
		for(Object x:jpoint.getArgs())
		{
			double v=(Double)x;
			if(v<0)
				throw new IllegalArgumentException("Number should be positive");
		}
		
	}
	
}
