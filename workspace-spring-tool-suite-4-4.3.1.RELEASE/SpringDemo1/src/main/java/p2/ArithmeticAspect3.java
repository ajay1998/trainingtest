package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspect3 {
	
	
	@Around("execution( * *.*(double,double))")
	public double check1(ProceedingJoinPoint jpoint) throws Throwable
	{
		double y=0;
		for(Object x:jpoint.getArgs())
		{
			double v=(Double)x;
			if(v<0)
				throw new IllegalArgumentException("Number should be positive from around");
		}
		Object retval=jpoint.proceed();
		y=(Double)retval;
		if(y>100)
			throw new IllegalArgumentException("Output should be positive from around");
		return y;
	}
	
}
