package p2;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Arithmetic ob;
			ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
			ob=(Arithmetic)context.getBean("arithmetic");
			ob.add(500,100);
			//System.out.println(ob.add(-5, 1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

}
