package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList {
 private List<StudentBean> list;

public StudentList() {
	super();
	list= new ArrayList<StudentBean>();
	list.add(new StudentBean("Ajay",21,"Satara"));
	list.add(new StudentBean("Rajat",22,"Paliwal"));
	list.add(new StudentBean("Chinmay",20,"Chimpi"));
}

public List<StudentBean> getList() {
	return list;
}

public void setList(List<StudentBean> list) {
	this.list = list;
}
 
 
}
