package com.example.demo;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@RequestMapping("/hello")
	public String display()
	{
		return "Welcome to Goa Singham";
	}
	@RequestMapping("/list")
	public List<StudentBean> display1()
	{
		return dao.getStuds();
	}
	
	@Autowired
	StudentDAO dao;
	
	@RequestMapping("/list/{name}")
	public List<StudentBean> getStudentList(@PathVariable String name)
	{
		return dao.getStudents(name);
	}
	@RequestMapping(method=RequestMethod.POST,value="/list")
	public void getInsertedStudent(@RequestBody StudentBean ob)
	{
	dao.insert(ob);
	}
}
