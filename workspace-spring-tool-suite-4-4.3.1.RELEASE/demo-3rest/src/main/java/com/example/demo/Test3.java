package com.example.demo;

import java.util.Scanner;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class Test3 {

	static final String URL_CREATE_EMPLOYEE = "http://10.151.60.205:8099/student";

	public static void main(String[] args) {               
		StudentBean newEmployee = new StudentBean();          
		System.out.println("enter name");               
		String name=Read.sc.next();                  
		HttpHeaders headers = new HttpHeaders();         
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);          
		headers.setContentType(MediaType.APPLICATION_JSON);              
		RestTemplate restTemplate = new RestTemplate();              
		// Data attached to the request.          
		HttpEntity<String> requestBody = new HttpEntity<>(name, headers);              
		// Send request with POST method.         
		restTemplate.exchange(URL_CREATE_EMPLOYEE,HttpMethod.DELETE, requestBody, String.class);              
		
		}
}
