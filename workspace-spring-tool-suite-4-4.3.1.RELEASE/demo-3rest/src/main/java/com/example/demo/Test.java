package com.example.demo;
import java.util.Arrays;
import java.util.Scanner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
public class Test {    
	static final String URL_EMPLOYEES = "http://10.151.60.205:8099/student";        
	public static void main(String[] args) {               
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
		// Request to return JSON format       
		headers.setContentType(MediaType.APPLICATION_JSON);     
		// headers.set("my_other_key", "my_other_value");       
		// HttpEntity<String>: To get result as String.       
		HttpEntity<StudentBean[]> entity = new HttpEntity<StudentBean[]>(headers);        
		// RestTemplate        
		RestTemplate restTemplate = new RestTemplate();     
		// Send request with GET method, and Headers.        
		ResponseEntity<StudentBean[]> response = restTemplate.exchange(URL_EMPLOYEES,            
				HttpMethod.GET, entity, StudentBean[].class);       
		StudentBean[] result = response.getBody();
		for(StudentBean ob : result)
		{    System.out.println(ob);}   
		}
	


	}
