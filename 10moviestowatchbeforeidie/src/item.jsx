import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";

class Item extends Component {
  state = {};

  render() {
    return (
      <React.Fragment>
        <center>
          <div className="card" style={{ width: 350, height: 600 }}>
            <img
              className="card-img-top"
              src={this.props.poster}
              height="500px"
              width="350px"
            />
            {this.props.name}
            <br />
            {this.props.year}
            <br />
            <button className="btn btn-primary" onClick={this.props.onadd}>
              ADD
            </button>
          </div>
        </center>
      </React.Fragment>
    );
  }
}

export default Item;
