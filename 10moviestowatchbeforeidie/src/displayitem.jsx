import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";

class Display extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <center>
          <div className="card" style={{ width: 350, height: 600 }}>
            <img
              className="card-img-top"
              src={this.props.poster}
              height="500px"
              width="350px"
            />
            {this.props.name}
            <br />
            {this.props.year}
            <br />
            <button onClick={this.props.onremove}>Remove</button>
          </div>
        </center>
      </React.Fragment>
    );
  }
}

export default Display;
