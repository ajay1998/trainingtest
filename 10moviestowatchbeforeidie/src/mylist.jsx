import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import Display from "./displayitem";
class MyList extends Component {
  remove = (name, year, poster) => {
    console.log("lol" + name);
    this.props.removemovie(name, year, poster);
  };
  render() {
    return (
      <React.Fragment>
        <br />
        <br />
        <br />
        <div className="card-columns">
          {/* {props.movies.length ? ( */}
          {this.props.movies.map(data => (
            <Display
              movies={this.props.movies}
              name={data.name}
              year={data.year}
              poster={data.poster}
              onremove={() => this.remove(data.name, data.year, data.poster)}
            />
          ))
          /* ) : (
          <h2>No such movie</h2>
        )} */
          }
        </div>
      </React.Fragment>
    );
  }
}

export default MyList;
