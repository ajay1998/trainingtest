import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Movies from "./movie";
import MyList from "./mylist";

class Navbar extends Component {
  state = {
    moviename: "",
    mymovies: [],
    dataset: []
  };
  removefinal = (name, year, poster) => {
    console.log(name);
    let { mymovies } = this.state;
    mymovies = mymovies.filter(data => data.poster !== poster);
    this.setState({ mymovies: mymovies });
  };
  addfinal = (sname, syear, sposter) => {
    console.log(sname);
    this.state.mymovies.push({
      name: sname,
      year: syear,
      poster: sposter
    });
    this.setState({
      mymovies: this.state.mymovies
    });
    console.log(this.state.mymovies);
  };

  findmovie(event) {
    event.preventDefault();
    this.state.moviename = event.target.moviename.value;
    this.setState({ moviename: this.state.moviename });

    fetch("http://www.omdbapi.com/?apikey=fd1550e9&s=" + this.state.moviename)
      .then(res => res.json())
      .then(data => {
        this.setState({ dataset: data.Search });
      })
      .catch(console.log);
    console.log(this.state.dataset);
  }
  render() {
    return (
      <React.Fragment>
        <Router>
          <nav className="navbar navbar-expand-lg  bg-dark">
            <Link className="ml-2" to="/search">
              Search
            </Link>

            <Link className="ml-2" to="/cart">
              MyMovies
            </Link>
          </nav>
          <Route exact path="/search" component={() => this.f1()} />
          <Route exact path="/cart" component={() => this.f2()} />
        </Router>
      </React.Fragment>
    );
  }
  f1() {
    return (
      <Movies
        data={this.state.dataset}
        mymovies={this.state.mymovies}
        onSubmit={this.findmovie.bind(this)}
        addmovie={this.addfinal}
      />
    );
  }
  f2() {
    return (
      <MyList movies={this.state.mymovies} removemovie={this.removefinal} />
    );
  }
}

export default Navbar;
