import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import Item from "./item";
class Movies extends Component {
  addmovieparent = (name, year, poster) => {
    this.props.addmovie(name, year, poster);
    console.log(name);
  };
  render() {
    return (
      <React.Fragment>
        <center>
          <form onSubmit={this.props.onSubmit} style={{ marginTop: 10 }}>
            <input
              type="text"
              name="moviename"
              placeholder="Enter movie name!"
            />
            <input type="submit" value="Search" />
          </form>
        </center>
        <br />
        <br />
        <div className="card-columns">
          {this.props.data.length
            ? this.props.data.map(data => (
                <Item
                  mymovies={this.props.mymovies}
                  name={data.Title}
                  year={data.Year}
                  poster={data.Poster}
                  onadd={() =>
                    this.addmovieparent(data.Title, data.Year, data.Poster)
                  }
                />
              ))
            : null}
        </div>
      </React.Fragment>
    );
  }
}

export default Movies;
