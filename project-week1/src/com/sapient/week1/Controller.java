package com.sapient.week1;

public class Controller {

	public static void main(String[] args) {
		Model mod = new Model();
		View view = new View();
		DAO dao = new DAO();
		view.read(mod);
		dao.compute(mod);
		view.display(mod);
		view=null;
		dao=null;
		mod=null;

	}

}
