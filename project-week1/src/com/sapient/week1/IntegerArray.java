package com.sapient.week1;

import java.util.Arrays;
import java.util.Scanner;

public class IntegerArray {
    private int size;
    private int array[];
    public static Scanner sc = new Scanner(System.in);
	public IntegerArray()
	{
		size=10;
		array=new int[10];
	}
	public IntegerArray(int size)
	{
		this.size=size;
		array=new int[this.size];
	}
	public IntegerArray(IntegerArray ob)
	{
	    this.size=ob.size;
		array=ob.array;
	}
	public void adopt(int ar[])
	{
		array=ar;
		display();
	}
	public void read()
	{
		for(int i=0;i<size;i++)
		{
			array[i]=IntegerArray.sc.nextInt();
		}
	}
	public void display()
	{
		for(int i=0;i<size;i++)
		{
			System.out.print(array[i]+"\t");
		}
		System.out.println();
	}
	public void search(int k)
	{
		
		int ind=Arrays.binarySearch(array, k);
		if(ind>=0)
		System.out.println("Index="+ind);
		else
	    System.out.println("Not found");
		
	}
	
	public void sortArray()
	{
		Arrays.sort(array);
		display();
	}
	public void arrayAverage()
	{
		int sum=0;
		for(int i=0;i<size;i++)
		{
			sum+=array[i];
		}
		sum=sum/size;
		System.out.println("Average="+sum);
	}
	public static void main(String[] args) {
		System.out.println("Enter size of array");
		int size=IntegerArray.sc.nextInt();
		IntegerArray ob = new IntegerArray(size);
		ob.read();
		ob.display();
		ob.sortArray();
		ob.search(5);
		ob.arrayAverage();
		IntegerArray nob= new IntegerArray(ob);
		nob.display();
		ob.adopt(new int[] {8,7,6,5,4,3,2,1});

	}

}
