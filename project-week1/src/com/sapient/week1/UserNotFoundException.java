package com.sapient.week1;

public class UserNotFoundException extends Exception{
	public UserNotFoundException()
	{
		super("UserNotFound");
		System.out.println("UserNotFound");
	}
	public UserNotFoundException(String message)
	{
		super(message);
	}
}
