package com.sapient.week1;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class EmployeeTestCases {

	@Test
	void testForReadData() throws FileNotFoundException, IOException {
		EmployeeDAO employee= new EmployeeDAO();
		List<EmployeeBean> list=new ArrayList<EmployeeBean>();
		list.add(new EmployeeBean(1,"Ajay",100000));
		list.add(new EmployeeBean(2,"Ajay1",200000));
		list.add(new EmployeeBean(3,"Ajay2",300000));
		Assert.assertArrayEquals(list.toArray(),employee.readData().toArray());
	}
	@Test
	void testforGetEmployee() throws FileNotFoundException, IOException, UserNotFoundException
	{
		EmployeeDAO employee= new EmployeeDAO();
		Assert.assertEquals(new EmployeeBean(1,"Ajay",100000),employee.getEmployee(1,"Ajay",100000));
	}
	@Test
	void testforGetEmployee1() throws FileNotFoundException, IOException
	{
		
		EmployeeDAO employee= new EmployeeDAO();
		try {
		Assert.assertEquals(new EmployeeBean(1,"Ajay",100000),employee.getEmployee(2,"Ajay",500000));
		
		}
		catch(UserNotFoundException e)
		{
			System.out.println(e);
		}
	}
	@Test
	void testforGetTotSalary() throws FileNotFoundException, IOException
	{
		EmployeeDAO employee= new EmployeeDAO();
		Assert.assertEquals(600000.0,employee.getTotalSalary(employee.readData()),0);;
	}
	

}
