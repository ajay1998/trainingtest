package com.sapient.week1;

public class PlayerBean {
	private int id;
	private String firstname;
	private String lastname;
	private int jerseyno;
	
	public PlayerBean() {
		super();
	}
	@Override
	public String toString() {
		return "PlayerBean [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", jerseyno=" + jerseyno
				+ "]\n";
	}
	public PlayerBean(int id, String firstname, String lastname, int jerseyno) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.jerseyno = jerseyno;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getJerseyno() {
		return jerseyno;
	}
	public void setJerseyno(int jerseyno) {
		this.jerseyno = jerseyno;
	}
		
}
