package com.sapient.week1;

import java.util.List;
import java.util.Scanner;

import java.util.Iterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Collection {

	public static void main(String[] args) {
		List<Employee> list = new ArrayList<Employee>(); 
		list.add(new Employee(3,"ajay",21));
		list.add(new Employee(1,"chinmay",22));
		list.add(new Employee(2,"rajat",20));
		Scanner sc = new Scanner(System.in);
		//System.out.println("Enter id of employee");
		System.out.println("Enter choice(name/age)");
		if(sc.next().equals("name"))
		{
			Collections.sort(list, new Comparator<Employee>() {

	        public int compare(Employee e1, Employee e2) {
	            
	            return e1.getName().compareTo(e2.getName());
	        }
			});
			System.out.println(list);
			
		}
		else
		{
			Collections.sort(list, new Comparator<Employee>() {

		    public int compare(Employee e1, Employee e2) {
		            
		    	return e1.getAge().compareTo(e2.getAge());
		    }
			});
			System.out.println(list);
			
		}
		
	}

	

	

}
