package com.sapient.week1;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

class EmployeeMockitoTest {
	private static EmployeeDAO mockemp;
	@BeforeAll
	public static void setUp() {
		System.out.println("ok");
		mockemp = Mockito.mock(EmployeeDAO.class);
		EmployeeBean emp1 =new EmployeeBean(1,"Ajay",1000);
		EmployeeBean emp2 =new EmployeeBean(2,"Rajat",2000);
		Mockito.when(mockemp.readData()).thenReturn(Arrays.asList(emp1,emp2));
	}
	@Test
    public void testGetEmployee()
    {
		
    	Assert.assertEquals(3000.0f,new EmployeeDAO().getTotalSalary(mockemp.readData()) ,0);
    
    }

}
