package com.sapient.week1;

public class View {
	public void read(Model mod)
	{
		System.out.println("Enter the two numbers");
		mod.setNum1(IntegerArray.sc.nextInt());
		mod.setNum2(IntegerArray.sc.nextInt());
	}
	public void display(Model mod)
	{
		System.out.println("Ans="+mod.getResult());
	}
}
