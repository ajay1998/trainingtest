package com.sapient.week1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PlayerDAO {
	public List<PlayerBean> getPlayers() throws Exception
	{
		Connection con=DBConnect.getConnection();
		PreparedStatement pstat = con.prepareStatement("select * from Players");
		ResultSet rs = pstat.executeQuery();
		List<PlayerBean> list = new ArrayList<PlayerBean>();
		while(rs.next())
		{
			list.add(new PlayerBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
			
		}
		return list;		
	}
	public List<PlayerBean> getAnyPlayer(int id) throws Exception
	{
		Connection con=DBConnect.getConnection();
		PreparedStatement pstat = con.prepareStatement("select * from Players where id=?");
		pstat.setInt(1, id);
		ResultSet rs = pstat.executeQuery();
		List<PlayerBean> list = new ArrayList<PlayerBean>();
		while(rs.next())
		{
			list.add(new PlayerBean(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
			
		}
		return list;		
	}
	public void insertPlayer(int id, String fname, String lname, int jersey ) throws Exception
	{
		Connection con=DBConnect.getConnection();
		PreparedStatement pstat = con.prepareStatement("insert into Players values(?,?,?,?)");
		pstat.setInt(1, id);
		pstat.setString(2, fname);
		pstat.setString(3, lname);
		pstat.setInt(4, jersey);
		pstat.execute();
	}
	public void updatePlayer(String fname, String lname, int jersey,int oldid ) throws Exception
	{
		Connection con=DBConnect.getConnection();
		PreparedStatement pstat = con.prepareStatement("update Players set firstname=?,lastname=?,jerseyno=? where id=?");
		pstat.setString(1, fname);
		pstat.setString(2, lname);
		pstat.setInt(3, jersey);
		pstat.setInt(4, oldid);
		pstat.execute();
		con.commit();
	}
	public void deletePlayer(int oldid ) throws Exception
	{
		Connection con=DBConnect.getConnection();
		PreparedStatement pstat = con.prepareStatement("delete from Players where id=?");
		pstat.setInt(1, oldid);
		pstat.execute();
		con.commit();
	}
}
