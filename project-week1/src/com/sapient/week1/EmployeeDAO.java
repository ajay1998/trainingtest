package com.sapient.week1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO {
public List<EmployeeBean> readData() 
{
//	BufferedReader bfr= new BufferedReader(new FileReader("C:\\Users\\ajasabal\\Desktop\\AjaySabale\\trainingtest\\trainingtest\\Employee.txt"));
//	String line;
//	List<EmployeeBean> list = new ArrayList<EmployeeBean>();
//	while((line=bfr.readLine())!=null)
//	{
//		String arr[]=line.split(",");
//		list.add(new EmployeeBean(Integer.parseInt(arr[0]),arr[1],Integer.parseInt(arr[2])));
//		
//	}
//	return list;
return null;
}
public EmployeeBean getEmployee(int id,String name, float salary) throws IOException, UserNotFoundException
{
	List<EmployeeBean> list=readData();
	EmployeeBean empob= new EmployeeBean(id,name,salary);
	EmployeeBean fob=null;
	for(EmployeeBean et : list)
	{
		if(et.equals(empob))
		{
			fob=new EmployeeBean(et.getId(),et.getName(),et.getSalary());
		}
	}
	if(fob==null)
		throw new UserNotFoundException();
	return fob;
	
	
}

public  float getTotalSalary(List<EmployeeBean> list) 
{
	
	float totalSalary=list.stream()
			.map((ep)->ep.getSalary())
			.reduce(0.0f,Float::sum);
	return totalSalary;
}
public int getCount(List<EmployeeBean> list,float salary)
{
      int totalCount=(int) list.stream()
    		  .filter((ep)->ep.getSalary()==salary)
    		  .count();
      return totalCount;
}
}
