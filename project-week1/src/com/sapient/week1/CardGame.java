package com.sapient.week1;

import java.util.Arrays;

class SyncArray{
	int arr[];
	int win[];
	SyncArray()
	{
		arr=new int[] {0,0,0};
		win=new int[3];
	}
}
class Game
{
	SyncArray sval;
	Game(SyncArray sval)
	{
		this.sval=sval;
	}
	public synchronized void g1()
	{
		try{
		Thread.sleep(1000);
		notifyAll();
		sval.win[0]=(int)(Math.random()*10+1);
		System.out.println("Player1:"+sval.win[0]+"\t");
		sval.arr[0]=1;
		wait();
	   }
		catch(InterruptedException e)
		{
			System.out.println("Interrupted");
		}
	}
	public synchronized void g2()
	{
		try{
			Thread.sleep(1000);
		notifyAll();
		sval.win[1]=(int)(Math.random()*10+1);
		System.out.println("Player2:"+sval.win[1]+"\t");
		sval.arr[1]=1;
		wait();
	   }
		catch(InterruptedException e)
		{
			System.out.println("Interrupted");
		}
	}
	public synchronized void g3()
	{
		try{
			Thread.sleep(1000);
		notifyAll();
		sval.win[2]=(int)(Math.random()*10+1);
		System.out.println("Player3:"+sval.win[2]+"\t");
		sval.arr[2]=1;
		wait();
	   }
		catch(InterruptedException e)
		{
			System.out.println("Interrupted");
		}
	}
}
class Player1 extends Thread
{
	Game game;
	SyncArray sval;
	public Player1(Game game, SyncArray sval)
	{
		this.game=game;
		this.sval=sval;
	}
	public void run()
	{
		for(int i=0;i<10;i++)
		{
			if(sval.arr[0]==0)
				game.g1();
				else
				try {
					if(sval.arr[1]==1 && sval.arr[2]==1)
					{
						if(sval.win[0]>sval.win[1])
						{
							if(sval.win[0]>sval.win[2])
							{
								System.out.println("Player1 wins 0");
							}
							else
							{
								System.out.println("Player3 wins 0");
							}
						}
						else
						{
							if(sval.win[1]>sval.win[2])
							{
								System.out.println("Player2 wins 0");
							}
							else
							{
								System.out.println("Player3 wins 0");
							}
						}
						sval.arr[0]=0;
						sval.arr[1]=0;
						sval.arr[2]=0;
						sval.win[0]=0;
						sval.win[1]=0;
						sval.win[2]=0;
						
						game.g1();
					}
					else
					{
						wait();
					}
				}
				catch(InterruptedException e) {}
		}
	}
}
class Player2 extends Thread
{
	Game game;
	SyncArray sval;
	public Player2(Game game, SyncArray sval)
	{
		this.game=game;
		this.sval=sval;
	}
	public void run()
	{
		for(int i=0;i<10;i++)
		{
			if(sval.arr[1]==0)
			game.g2();
			else
			try {
				if(sval.arr[0]==1 && sval.arr[2]==1)
				{
					if(sval.win[0]>sval.win[1])
					{
						if(sval.win[0]>sval.win[2])
						{
							System.out.println("Player1 wins 1");
						}
						else
						{
							System.out.println("Player3 wins 1");
						}
					}
					else
					{
						if(sval.win[1]>sval.win[2])
						{
							System.out.println("Player2 wins 1");
						}
						else
						{
							System.out.println("Player3 wins 1");
						}
					}
					sval.arr[0]=0;
					sval.arr[1]=0;
					sval.arr[2]=0;
					sval.win[0]=0;
					sval.win[1]=0;
					sval.win[2]=0;
					
					game.g2();
				}
				else
				{
					wait();
				}
			}
			catch(InterruptedException e) {}
		}
	}
}
class Player3 extends Thread
{
	Game game;
	SyncArray sval;
	public Player3(Game game, SyncArray sval)
	{
		this.game=game;
		this.sval=sval;
	}
	public void run()
	{
		for(int i=0;i<10;i++)
		{
			if(sval.arr[2]==0)
				{
				
				game.g3();
				}
				else
				try {
					if(sval.arr[1]==1 && sval.arr[0]==1)
					{
						if(sval.win[0]>sval.win[1])
						{
							if(sval.win[0]>sval.win[2])
							{
								System.out.println("Player1 wins 3");
							}
							else
							{
								System.out.println("Player3 wins 3");
							}
						}
						else
						{
							if(sval.win[1]>sval.win[2])
							{
								System.out.println("Player2 wins 3");
							}
							else
							{
								System.out.println("Player3 wins 3");
							}
						}
						sval.arr[0]=0;
						sval.arr[1]=0;
						sval.arr[2]=0;
						sval.win[0]=0;
						sval.win[1]=0;
						sval.win[2]=0;
						
						game.g3();
					}
					else
					{
						wait();
					}
				}
				catch(InterruptedException e) {}
		}
	}
}
public class CardGame {
	
	
	public static void main(String args[])
	{
		
		
		SyncArray sval=new SyncArray();
		Game game=new Game(sval);
		Player1 p1 =new Player1(game,sval);
		Player2 p2 =new Player2(game,sval);
		Player3 p3 =new Player3(game,sval);
	    p1.start();
	    p2.start();
	    p3.start();
	}

}
