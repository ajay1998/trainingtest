package com.sapient.week1;

public class FigureToWords {
	String word="";
	String unit[]= {"","One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"};
	String tens[]= {"","","Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety"};
    String vunit[]= {"Crores","Lakhs","Thousands","Hundreds","Only"};
    long nunit[]= {10000000L,100000L,1000L,100L,1L};
    
    public void convert(long amt)
    {
    	int i;
    	for(i=0;i<vunit.length;i++)
    	{
    		int val=(int)(amt/nunit[i]);
    		amt=amt%nunit[i];
    		if(val>19)
    		{
    			word+=tens[val/10]+""+unit[val%10]+""+vunit[i];
    		}
    		else if(val>0)
    			word+=unit[val]+""+vunit[i];
    	}
    	
    	System.out.println(word);
    }
    
}
